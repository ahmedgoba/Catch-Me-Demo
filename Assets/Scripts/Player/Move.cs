﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class Move : NetworkBehaviour {

	public float walkSpeed = 0.05f;
	public float runSpeed = 0.1f;
	public float crouchSpeed=0.02f;
	public float CurrentSpeed=0.0f;
	float animationSpeedPercent = 0.0f;
	float HiderSpeedFactor=1.25f;
	float JumpTime=1.0f;
	public float Range=0.4f,BackRange=1.0f;
	public float forceConst = 2.0f;
	bool jumping=false;
	Rigidbody selfRigidbody;
	[SerializeField] Camera Cam;
	RaycastHit Hit;
	[SerializeField] LayerMask Mask;

	[SerializeField] GameObject HunterCam,HiderCam;
	[SerializeField] Animator animator;
	[SerializeField] AudioSource RunSound;
	// Use this for initialization
	void Start () {

		selfRigidbody = GetComponent<Rigidbody>();
		animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(PauseMenu.IsOn)
			return;
			
		if(jumping)
		{
			JumpAnimation();
		}
		CurrentSpeed=walkSpeed;
		animationSpeedPercent=1.0f;
		if(GetComponent<PlayerSetup>().isHider)
		{
			
			if(Input.GetKey(KeyCode.LeftShift)&&Input.GetKey(KeyCode.W))//Run
			{
				if(!Physics.Raycast(HiderCam.transform.position,HiderCam.transform.forward,Range,Mask))
				{
					if(!RunSound.isPlaying&&transform.position.y<0.4f)
					{
						RunSound.volume=Random.Range(0.5f,0.8f);
						RunSound.pitch=Random.Range(0.8f,1.2f);
						RunSound.Play();
					}
					animationSpeedPercent=3.0f;
					transform.Translate(transform.forward * runSpeed*HiderSpeedFactor, Space.World);
					CurrentSpeed=runSpeed;
				}
				// Cam.transform.localPosition=Vector3.zero;
				// Cam.transform.Translate(Vector3.forward*3);
				// Debug.Log(Cam.transform.localPosition);
			}
			else if(Input.GetKey(KeyCode.W))//Walk
			{
				if(!Physics.Raycast(HiderCam.transform.position,HiderCam.transform.forward,Range,Mask))
				{
					animationSpeedPercent=2.0f;
					transform.Translate(transform.forward * walkSpeed*HiderSpeedFactor, Space.World);
					CurrentSpeed=walkSpeed;
				}
				// Cam.transform.localPosition=Vector3.zero;
				// Cam.transform.Translate(Vector3.forward*2);
				// Debug.Log(Cam.transform.localPosition);
			}
			// else //idle
			// {
			// 	Cam.transform.localPosition=Vector3.zero;
			// }
		
			if(Input.GetKey(KeyCode.D))//Right
			{
				if(!Physics.Raycast(HiderCam.transform.position,HiderCam.transform.right,Range,Mask))
				{
					animationSpeedPercent=4.0f;
					transform.Translate(transform.right * CurrentSpeed*HiderSpeedFactor, Space.World);
				}			
			}
			if(Input.GetKey(KeyCode.A))//Left
			{
				if(!Physics.Raycast(HiderCam.transform.position,-HiderCam.transform.right,Range,Mask))
				{
					animationSpeedPercent=5.0f;
					transform.Translate(-transform.right * CurrentSpeed*HiderSpeedFactor, Space.World);
				}
			}
			if(Input.GetKey(KeyCode.S))//Walk BackWard
			{
				// Debug.Log(Cam.transform.forward+" + "+Cam.transform.forward*0.5f);
				if(!Physics.Raycast(HiderCam.transform.position,-HiderCam.transform.forward,Range+0.3f,Mask))
				{
					animationSpeedPercent=6.0f;
					transform.Translate(-transform.forward * CurrentSpeed*HiderSpeedFactor, Space.World);
				}		
			}
			
			animator.SetFloat ("speedPercent", -animationSpeedPercent);

			if(Input.GetKeyDown(KeyCode.Space)&&!jumping)
			{	
				animator.SetBool("HiderJump",true);
				JumpAnimation();
				jumping=true;
				selfRigidbody.AddForce(0, forceConst,0, ForceMode.Impulse);
				
			}

			//Hiders Win
			if(transform.position.x>=-2.5&&transform.position.x<=2.5&&transform.position.z>=49.0f)
			{
				GetComponent<GameControl>().CmdHidersWin();
			}
		}
		else
		{
			if(Input.GetKey(KeyCode.LeftShift)&&Input.GetKey(KeyCode.W))//Run
			{
				if(!Physics.Raycast(HunterCam.transform.position,HunterCam.transform.forward,Range,Mask))
				{
					if(!RunSound.isPlaying&&transform.position.y<0.4f)
					{
						RunSound.volume=Random.Range(0.5f,0.8f);
						RunSound.pitch=Random.Range(0.8f,1.2f);
						RunSound.Play();
					}
					animationSpeedPercent=3.0f;
					transform.Translate(transform.forward * runSpeed, Space.World);
					CurrentSpeed=runSpeed;
				}
				// if(Cam.transform.localPosition.z<=1.0f)
				// Cam.transform.Translate(Vector3.forward*2);
				// Debug.Log(Cam.transform.localPosition);
			}
			else if(Input.GetKey(KeyCode.W))//Walk
			{
				if(!Physics.Raycast(HunterCam.transform.position,HunterCam.transform.forward,Range,Mask))
				{
					animationSpeedPercent=2.0f;
					transform.Translate(transform.forward * walkSpeed, Space.World);
					CurrentSpeed=walkSpeed;
				}
				// if(Cam.transform.localPosition.z<=0.5f)
				// Cam.transform.Translate(Vector3.forward);
				// Debug.Log(Cam.transform.localPosition);
			}
			// else //idle
			// {
			// 	Cam.transform.localPosition=Vector3.zero;	
			// }
		
			if(Input.GetKey(KeyCode.D))//Right
			{
				if(!Physics.Raycast(HunterCam.transform.position,HunterCam.transform.right,Range,Mask))
				{
					animationSpeedPercent=4.0f;
					transform.Translate(transform.right * CurrentSpeed, Space.World);
				}		
			}
			if(Input.GetKey(KeyCode.A))//Left
			{
				if(!Physics.Raycast(HunterCam.transform.position,-HunterCam.transform.right,Range,Mask))
				{
					animationSpeedPercent=5.0f;
					transform.Translate(-transform.right * CurrentSpeed, Space.World);
				}
			}
			if(Input.GetKey(KeyCode.S))//Walk BackWard
			{
				if(!Physics.Raycast(HunterCam.transform.position,-HunterCam.transform.forward,Range+0.25f,Mask))
				{
					animationSpeedPercent=6.0f;
					transform.Translate(-transform.forward * CurrentSpeed, Space.World);
				}				
			}
			
			animator.SetFloat ("speedPercent", animationSpeedPercent);

			if(Input.GetKeyDown(KeyCode.Space)&&!jumping)
			{	
				animator.SetBool("HunterJump",true);
				jumping=true;
				JumpAnimation();
				selfRigidbody.AddForce(0, forceConst,0, ForceMode.Impulse);
				
			}
		}
	}
	void JumpAnimation()
	{		
		JumpTime-=Time.deltaTime;
		if(JumpTime<=0)
		{
			JumpTime=1.0f;
			jumping=false;
			animator.SetBool("HunterJump",false);
			animator.SetBool("HiderJump",false);
		}
		
	}
}

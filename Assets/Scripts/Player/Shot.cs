﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Shot : NetworkBehaviour {

	[SerializeField] private Camera Cam;
	private NetworkInstanceId Player1Id,Player2Id;
	public float HunterRange,HiderRange,HunterAbbilityCoolDown=6.0f,HiderAbbilityCoolDown=5.0f;
	[SerializeField] GameObject LandingCircle;
	public GameObject Rope;
	[SerializeField] GameObject GrenadePref,GrenadePosition=null;
	bool MousePressed,inCoolDown,throwing;
	RaycastHit Hit;
	[SerializeField] private GameObject Weapon;
	[SerializeField] LayerMask Mask;
	[SerializeField] Animator animator;
	[SerializeField] AudioSource ShotSound,ThrowSound;
	float ThrowAnimationTime=1.0f;

	// Use this for initialization
	void Start () {
		MousePressed=inCoolDown=throwing=false;
	}
	
	// Update is called once per frame
	void Update () {

		if(PauseMenu.IsOn)
			return;
			
		if(!GetComponent<PlayerSetup>().isHider)
		{
			if(inCoolDown)
				return;
			if(Input.GetKeyDown(KeyCode.Mouse0))
			{
				if(Physics.Raycast(Cam.transform.position,Cam.transform.forward,out Hit,HunterRange,Mask))
				{
					ShotSound.Play();
					inCoolDown=true;
					StartCoroutine(AbilityCoolDown());
					Player1Id=GetComponent<NetworkIdentity>().netId;
					Player2Id=Hit.transform.gameObject.GetComponent<NetworkIdentity>().netId;
					GetComponent<Move>().enabled=false;
					Cam.GetComponent<look>().enabled=false;
					CmdHunterShot(Hit.point,Player1Id,Player2Id);
				}
			}
		}
		else
		{
			if(throwing)
			{
				ThrowAnimation();
				return;
			}
			if(inCoolDown)
				return;

			if(Input.GetKeyDown(KeyCode.Mouse0) && !MousePressed)//Launch Shot
			{
				LandingCircle.SetActive(true);
				MousePressed=true;
			}
			if(MousePressed)//Hit Marker On the Ground
			{
				
				if(Physics.Raycast(Cam.transform.position,Cam.transform.forward,out Hit,HiderRange))
				{				
					LandingCircle.transform.position=new Vector3(Hit.point.x,0.1f,Hit.point.z);
				}
			}

			if(Input.GetKeyDown(KeyCode.Mouse1) && MousePressed)//Cancel Shot 
			{
				LandingCircle.SetActive(false);
				MousePressed = false;
			}

			if(Input.GetKeyUp(KeyCode.Mouse0) && MousePressed)//Shot Grenade
			{
				throwing=true;
				animator.SetBool("Throw",true);
				ThrowAnimation();
				StartCoroutine(Throw());
				LandingCircle.SetActive(false);
				MousePressed=false;
				inCoolDown=true;
				StartCoroutine(AbilityCoolDown());
			}
		}
	}
	IEnumerator Throw()
	{
		yield return new WaitForSeconds(0.25f);
		ThrowSound.Play();
		GameObject Grenade=(GameObject)Instantiate(GrenadePref,GrenadePosition.transform.position,Quaternion.identity);
		NetworkServer.Spawn(Grenade);
		//Physics.IgnoreCollision(Grenade.GetComponent<Collider>(),GetComponent<Collider>(),true);
		Vector3 throwSpeed = calculateBestThrowSpeed(GrenadePosition.transform.position,new Vector3(Hit.point.x,0.1f,Hit.point.z),0.8f);
		Grenade.GetComponent<Rigidbody>().AddForce(throwSpeed, ForceMode.VelocityChange);

		yield return new WaitForSeconds(0.50f);
		//Physics.IgnoreCollision(Grenade.GetComponent<Collider>(),GetComponent<Collider>(),false);
		yield return new WaitForSeconds(0.35f);		
		GetStunnedPlayers(Grenade.GetComponent<Grenade>().StunnedPlayers());
		
		yield return new WaitForSeconds (2f);
		
		Destroy(Grenade);
	}
	void ThrowAnimation()
	{
		ThrowAnimationTime-=Time.deltaTime;
		if(ThrowAnimationTime<=0)
			{
				ThrowAnimationTime=1.0f;
				throwing=false;
				animator.SetBool("Throw",false);
			}
	}
	void GetStunnedPlayers(List<GameObject> Players)
	{
		for(int x=0;x<Players.Count;x++)
		{
			CmdStunPlayer(Players[x].GetComponent<NetworkIdentity>().netId);
		}
	}

	[Command]
	void CmdStunPlayer(NetworkInstanceId playerid)
	{
		RpcStunPlayer(playerid);
	}
	[ClientRpc]
	void RpcStunPlayer(NetworkInstanceId playerid)
	{
		((GameObject)ClientScene.FindLocalObject(playerid)).GetComponent<PlayerSetup>().Stun(playerid);
	}
	IEnumerator AbilityCoolDown()
	{
		if(GetComponent<PlayerSetup>().isHider)
		{
			yield return new WaitForSeconds(8.0f);
			inCoolDown=false;
		}
		else
		{
			yield return new WaitForSeconds(8.5f);
			inCoolDown=false;
		}
	}
	Vector3 calculateBestThrowSpeed(Vector3 origin, Vector3 target, float timeToTarget) 
	{
		// calculate vectors
		Vector3 toTarget = target - origin;
		Vector3 toTargetXZ = toTarget;
		toTargetXZ.y = 0;
	
		// calculate xz and y
		float y = toTarget.y;
		float xz = toTargetXZ.magnitude;
	
		// calculate starting speeds for xz and y. Physics forumulase deltaX = v0 * t + 1/2 * a * t * t
		// where a is "-gravity" but only on the y plane, and a is 0 in xz plane.
		// so xz = v0xz * t => v0xz = xz / t
		// and y = v0y * t - 1/2 * gravity * t * t => v0y * t = y + 1/2 * gravity * t * t => v0y = y / t + 1/2 * gravity * t
		float t = timeToTarget;
		float v0y = y / t + 0.5f * Physics.gravity.magnitude * t;
		float v0xz = xz / t;
	
		// create result vector for calculated starting speeds
		Vector3 result = toTargetXZ.normalized;        // get direction of xz but with magnitude 1
		result *= v0xz;                                // set magnitude of xz to v0xz (starting speed in xz plane)
		result.y = v0y;                                // set y to v0y (starting speed of y plane)
	
		return result;
	}
	[Command]
	void CmdHunterShot(Vector3 point,NetworkInstanceId player1_id,NetworkInstanceId player2_id)
	{				
		RpcHunterShot(point,player1_id,player2_id);
	}
	[ClientRpc]
	void RpcHunterShot(Vector3 point,NetworkInstanceId player1_id,NetworkInstanceId player2_id)
	{
		
		GameObject Player1=(GameObject)ClientScene.FindLocalObject(player1_id);
		GameObject Player2=(GameObject)ClientScene.FindLocalObject(player2_id);

		point=Player2.GetComponent<Shot>().Rope.transform.position;

		LineRenderer LR1=Player1.GetComponent<LineRenderer>();
		
		LR1.enabled=true;
		LR1.SetPosition(1,point);
		LR1.SetPosition(0,Weapon.transform.position);
		Player2.GetComponent<Shot>().Rope.SetActive(true);

		LineRenderer LR2=Player2.GetComponent<LineRenderer>();

		
		
		//Stop Player1
		StartCoroutine(Player1.GetComponent<PlayerSetup>().Catch(LR1,Weapon));

		//Stop Player2
		StartCoroutine(Player2.GetComponent<PlayerSetup>().Catch(LR2,Weapon));

	}

}

﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class look : NetworkBehaviour {

    [SerializeField] GameObject Player;
	[SerializeField] Animator animator;	
	[SerializeField] InputField inputfield;
	[SerializeField] GameObject HunterHead,HiderHead;
	public float mouseSensitivity = 3.0f;
	public float clampAngle = 80.0f;
	float mouseX,mouseY;
	private float rotY = 0.0f; // rotation around y axis
	private float rotX = 0.0f; // rotation around x axis
	float current_rotation;
	void Start ()
	{
		inputfield.text=Math.Round(mouseSensitivity,2,MidpointRounding.AwayFromZero).ToString();
		Vector3 rot = transform.localRotation.eulerAngles;
		
		rotY = rot.y;
		rotX = rot.x;
	}

	void Update ()
	{
		if(PauseMenu.IsOn)
			return;

		mouseX = Input.GetAxis("Mouse X");
        mouseY = -Input.GetAxis("Mouse Y");
 
        rotY += mouseX * mouseSensitivity;
        rotX += mouseY * mouseSensitivity;

        rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);
		
        Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
        //Set Rotation
		transform.rotation = localRotation;
		//Set Position
		if(Player.GetComponent<PlayerSetup>().isHider&&Player.GetComponent<PlayerSetup>().HiderPrefab.activeSelf)
			transform.position=HiderHead.transform.position+(transform.forward*0.2f);
		else
			transform.position=HunterHead.transform.position+(transform.forward*0.2f);
		Player.transform.Rotate(0.0f,mouseX*mouseSensitivity, 0.0f);


		if(rotX<=0)//looking up
		{
			animator.SetFloat("AimAngle",-rotX, 0.1f, Time.deltaTime);
		}
		else //looking down
		{
			animator.SetFloat("AimAngle",-rotX, 0.1f, Time.deltaTime);
		}
		//transform.localRotation = Quaternion.identity;
	}
	public void SetMouseSensitivity()
	{
		float Sensitivity=0;

		if (float.TryParse(inputfield.text, out Sensitivity))
		{
			if(Sensitivity>100.0f)
			{
				mouseSensitivity=100.0f;
			}
			else if(Sensitivity<0.0f)
				mouseSensitivity=1.0f;
			else
			{
				mouseSensitivity=Sensitivity;
				inputfield.text=Math.Round(mouseSensitivity,2,MidpointRounding.AwayFromZero).ToString();
			}
		}
		else
		{
			inputfield.text=Math.Round(mouseSensitivity,2,MidpointRounding.AwayFromZero).ToString();
		}
	}
}

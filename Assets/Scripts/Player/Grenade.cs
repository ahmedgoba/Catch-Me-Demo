﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class Grenade : NetworkBehaviour {
	public float radius=6.5f;
	List <GameObject> PlayersToStun=new List<GameObject>();
	GameObject [] AllPlayers;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnCollisionEnter(Collision collision)
    {
    	gameObject.GetComponent<Rigidbody>().isKinematic=true;
    	gameObject.GetComponent<Rigidbody>().angularVelocity=Vector3.zero;
		AllPlayers=GameObject.FindGameObjectsWithTag("Player");
		for(int x=0;x<AllPlayers.Length;x++)
		{
			if(CalculateDistance(AllPlayers[x].transform.position,transform.position)<=6.5f)
			{
				PlayersToStun.Add(AllPlayers[x]);
			}
		}
    }
	float CalculateDistance(Vector3 vector1,Vector3 vector2)
	{
		Debug.Log("Vector1 : "+vector1+" Vector2 : "+vector2);
		float xDistance=(vector2.x-vector1.x)*(vector2.x-vector1.x);
		float yDistance=(vector2.y-vector1.y)*(vector2.y-vector1.y);
		float zDistance=(vector2.z-vector1.z)*(vector2.z-vector1.z);
		Debug.Log(Mathf.Sqrt(xDistance+yDistance+zDistance));
		return Mathf.Sqrt(xDistance+yDistance+zDistance);
	}
	public List<GameObject> StunnedPlayers()
	{
		return PlayersToStun;
	}
}

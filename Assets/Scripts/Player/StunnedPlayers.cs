﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunnedPlayers : MonoBehaviour {

	List <GameObject> currentCollisions = new List <GameObject> ();
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnCollisionEnter (Collision col) {
		Debug.Log(col.gameObject.tag);
		
		// Add the GameObject collided with to the list.
		if(col.gameObject.tag=="Player")
			currentCollisions.Add (col.gameObject);

		// Print the entire list to the console.
		// foreach (GameObject gObject in currentCollisions) {
		// 	Debug.Log(gObject);
		//}
    }

	public List<GameObject> GetStunnedPlayers()
	{
		return currentCollisions;
	}
}

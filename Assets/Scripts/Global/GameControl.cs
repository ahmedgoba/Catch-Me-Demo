﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using System;
public class GameControl : NetworkBehaviour {

	public  bool isGameStarted,isGameReady,isGameEnded;
	[SerializeField] GameObject ClassChangeDialog;
	GameObject [] AllPlayers;
	List<string> FirstHunters,FirstHiders,FinalHunters,FinalHiders;
	string [] hiders,hunters;
	public int NumOfHunters,NumOfHiders;
	public bool GameEnded,HidersWin;
	public float StartTime=30,StartTimeVar;
	int MinimumPlayers=2;
	[SerializeField] GameObject GameMessagesCanvas;
	[SerializeField] Text Message;
	[SerializeField] Text StartTimeText;
	public Canvas StartTimeCanvas;
	float CountdownTime;


	// Use this for initialization
	void Start () {
		GameEnded=false;
		StartTimeVar=StartTime;
		isGameStarted=false;
		isGameReady=false;
		isGameEnded=false;
		FirstHiders=new List<string>();
		FirstHunters=new List<string>();
		FinalHiders=new List<string>();
		FinalHunters=new List<string>();
		NumOfHiders=-1;
		NumOfHunters=-1;
		HidersWin=false;
	}
	
	// Update is called once per frame
	void Update () {
		

		if(isGameStarted)
		{
			if(!isGameEnded)
			{
				if(NumOfHiders==0)
				{
					isGameEnded=true;
					StartCoroutine(GameWinner("HT"));
				}
				else if(HidersWin)
				{
					isGameEnded=true;
					StartCoroutine(GameWinner("HD"));
				}
			}
			else if (isGameEnded)
			{

			}
		}
		else
		{
			AllPlayers=GameObject.FindGameObjectsWithTag("Player");
			if(!isGameReady && AllPlayers.Length>=MinimumPlayers)
			{
				StartTimeVar-=Time.deltaTime;
				StartTimeText.text="Starting New Game in "+((int)StartTimeVar)+"s";
				if(StartTimeVar<=0)
				{
					isGameReady=true;
					if(isServer)
					{
						PrepareToStart();
					}
				}
			}
			else if(!isGameReady && AllPlayers.Length<MinimumPlayers)
			{
				StartTimeVar=StartTime;
				StartTimeText.text="Waiting For Players ( 4 minimum players )";
			}
		}
	}
	void PrepareToStart()
	{
		FirstHiders.Clear();
		FirstHunters.Clear();
		FinalHiders.Clear();
		FinalHunters.Clear();
		int HiderCount=AllPlayers.Length/3;
		Debug.Log("Prepare To Start , AllPlayers : "+AllPlayers.Length+" HiderCount : "+HiderCount);
		//AllPlayers=GameObject.FindGameObjectsWithTag("Player");
		for(int x=0;x<AllPlayers.Length;x++)
		{
			if(AllPlayers[x].GetComponent<PlayerSetup>().isHider)
				FirstHiders.Add(AllPlayers[x].GetComponent<NetworkIdentity>().netId.ToString());
			else
				FirstHunters.Add(AllPlayers[x].GetComponent<NetworkIdentity>().netId.ToString());
		}
		//Detect Hiders
		if(HiderCount==FirstHiders.Count)
		{
			for(int x=0;x<HiderCount;x++)
			{
				FinalHiders.Add(FirstHiders[x]);
			}
		}
		else if(HiderCount>FirstHiders.Count)//not enough Hiders
		{
			for(int x=0;x<FirstHiders.Count;x++)
			{
				FinalHiders.Add(FirstHiders[x]);
			}

			for(int x=0;x<HiderCount-FirstHiders.Count;x++)
			{
				int chosenHider=Random.Range(0,FirstHunters.Count-1);
				FinalHiders.Add(FirstHunters[chosenHider]);
				FirstHunters.RemoveAt(chosenHider);
			}

		}
		else // So many Hiders
		{
			for(int x=0;x<HiderCount;x++)
			{
				int chosenHider=Random.Range(0,FirstHiders.Count-1);
				FinalHiders.Add(FirstHiders[chosenHider]);
				FirstHiders.RemoveAt(chosenHider);
			}
			for(int x=0;x<FirstHiders.Count;x++)	
			{
				FinalHunters.Add(FirstHiders[x]);
			}

		}
		//Detect Hunters
		for(int x=0;x<FirstHunters.Count;x++)
		{
			FinalHunters.Add(FirstHunters[x]);
		}

		//send lists as arrays
		hiders =new string[FinalHiders.Count];
		hunters=new string[FinalHunters.Count];
		for(int x=0;x<FinalHunters.Count;x++)
		{
			hunters[x]=FinalHunters[x];
		}
		
		for(int x=0;x<FinalHiders.Count;x++)
		{
			hiders[x]=FinalHiders[x];
		}
		RpcSendChosenClass(hiders,hunters);
	}

	[Command]
	public void CmdSendChosenClass(NetworkIdentity networkIdentity,bool ishider)
	{
		GameObject Player = ClientScene.FindLocalObject(networkIdentity.netId);
		Player.GetComponent<PlayerSetup>().isHider=ishider;
	}
	[ClientRpc]
	public void RpcSendChosenClass(string [] Hiders,string [] Hunters)
	{
		foreach(GameObject Cage in GameObject.FindGameObjectsWithTag("Cage"))
		{
			Destroy(Cage);
		}
		StartTimeCanvas.enabled=false;
		NumOfHiders=Hiders.Length;
		NumOfHunters=Hunters.Length;

		AllPlayers=GameObject.FindGameObjectsWithTag("Player");
		for(int x=0;x<AllPlayers.Length;x++)
		{
			AllPlayers[x].GetComponent<GameControl>().StartTimeCanvas.enabled=false;
			if(AllPlayers[x].GetComponent<PlayerSetup>().inSpectate)
				continue;
			int c=0;
			for(int i=0;i<Hunters.Length;i++)
			{
				if(AllPlayers[x].GetComponent<NetworkIdentity>().netId.ToString()==Hunters[i])
				{
					AllPlayers[x].GetComponent<PlayerSetup>().isHider=false;
					AllPlayers[x].GetComponent<GameControl>().NumOfHiders=NumOfHiders;
					AllPlayers[x].GetComponent<GameControl>().NumOfHunters=NumOfHunters;
					AllPlayers[x].GetComponent<PlayerSetup>().ChangeClass();
					StartCoroutine(AllPlayers[x].GetComponent<PlayerSetup>().StartGame(i));
					c++;
					break;
				}
			}
			if(c>0)
				continue;
			for(int i=0;i<Hiders.Length;i++)
			{
				if(AllPlayers[x].GetComponent<NetworkIdentity>().netId.ToString()==Hiders[i])
				{
					AllPlayers[x].GetComponent<PlayerSetup>().isHider=true;
					AllPlayers[x].GetComponent<GameControl>().NumOfHiders=NumOfHiders;
					AllPlayers[x].GetComponent<GameControl>().NumOfHunters=NumOfHunters;
					AllPlayers[x].GetComponent<PlayerSetup>().ChangeClass();
					StartCoroutine(AllPlayers[x].GetComponent<PlayerSetup>().StartGame(i));
					break;
				}
			}		
		}	
	}

	[Command]
	public void CmdHiderDead(NetworkIdentity networkIdentity)
	{
		RpcHiderDead(networkIdentity);
	}
	[ClientRpc] // am i need to loop ? yes
	void RpcHiderDead(NetworkIdentity networkIdentity)
	{
		AllPlayers=GameObject.FindGameObjectsWithTag("Player");
		for(int x=0;x<AllPlayers.Length;x++)
		{
			AllPlayers[x].GetComponent<GameControl>().NumOfHiders--;
		}		
	}

	[Command]
	public void CmdHidersWin()
	{
		RpcHidersWin();
	}
	[ClientRpc]
	void RpcHidersWin()
	{
		AllPlayers=GameObject.FindGameObjectsWithTag("Player");
		for(int x=0;x<AllPlayers.Length;x++)
		{
			AllPlayers[x].GetComponent<GameControl>().HidersWin=true;
		}
	}
	IEnumerator GameWinner(string Winner)
	{
		GameMessagesCanvas.SetActive(true);
		if(Winner == "HT")
		Message.text = "Hunters Win!";
		else if(Winner == "HD")
		Message.text = "Hiders Win!";
		// GetComponent<PlayerUI>().StartTime.text+=" Starting New Game";
		// CountdownTime=20.0f;
		// InvokeRepeating ("Countdown", 1.0f, 1.0f);
		GameObject MapObjects=GameObject.FindGameObjectWithTag("Map");
		MapObjects.layer=LayerMask.NameToLayer("Light");
		foreach(Transform GO in MapObjects.GetComponentsInChildren<Transform>())
		{
			GO.gameObject.layer=LayerMask.NameToLayer("Light");
		}
		yield return new WaitForSeconds(10);
		StartNewGame();
	}
	void Countdown () {
		CountdownTime--;
		StartTimeText.text="Starting New Game in "+CountdownTime+"s";
		if (CountdownTime <= 0)
			CancelInvoke ("Countdown");
	}
	void StartNewGame()
	{
		foreach(GameObject Cage in GameObject.FindGameObjectsWithTag("Cage"))
		{
			Destroy(Cage);
		}
		GameMessagesCanvas.SetActive(false);
		HidersWin=false;
		AllPlayers=GameObject.FindGameObjectsWithTag("Player");
		for(int x=0;x<AllPlayers.Length;x++)
		{
			if(AllPlayers[x].GetComponent<PlayerSetup>().inSpectate)
			{
				AllPlayers[x].GetComponent<PlayerSetup>().inSpectate=false;
				AllPlayers[x].GetComponent<PlayerSetup>().DeactivateSpectateMode();
			}
			// if(AllPlayers[x].GetComponent<PlayerSetup>().isHider)
			// {
			// 	AllPlayers[x].GetComponent<PlayerSetup>().isHider=false;
			// 	AllPlayers[x].GetComponent<PlayerSetup>().ChangeClass();
			// 	AllPlayers[x].GetComponent<PlayerSetup>().isHider=true;
			// }
			// else
			// 	AllPlayers[x].GetComponent<PlayerSetup>().ChangeClass();
		}
		
		StartTimeCanvas.enabled=true;
		StartTimeVar=60;
		isGameEnded=false;
		isGameStarted=false;
		isGameReady=false;
	}
	[Command]
	public void CmdSpectator(NetworkIdentity networkIdentity)
	{
		ClientScene.FindLocalObject(networkIdentity.netId).GetComponent<PlayerSetup>().inSpectate=true;
		AllPlayers=GameObject.FindGameObjectsWithTag("Player");
		for(int x=0;x<AllPlayers.Length;x++)
		{
			if(AllPlayers[x].GetComponent<NetworkIdentity>().isLocalPlayer)
			{
				TargetSpectator(connectionToClient,AllPlayers[x].GetComponent<GameControl>().hiders,
				AllPlayers[x].GetComponent<GameControl>().hunters);

				RpcSpectator(networkIdentity);
				break;
			}
		}
	}
	
	[TargetRpc]
	void TargetSpectator(NetworkConnection target,string [] Hiders,string [] Hunters)
	{
		AllPlayers=GameObject.FindGameObjectsWithTag("Player");
		for(int x=0;x<AllPlayers.Length;x++)
		{
			AllPlayers[x].GetComponent<GameControl>().NumOfHiders=Hiders.Length;
			AllPlayers[x].GetComponent<GameControl>().NumOfHunters=Hunters.Length;
			if(AllPlayers[x].GetComponent<NetworkIdentity>().isLocalPlayer)
			{
				continue;
			}
			
			if(AllPlayers[x].GetComponent<PlayerSetup>().inSpectate)
			{
				AllPlayers[x].GetComponent<PlayerSetup>().ChangeClassSpectator();
				continue;
			}
			int c=0;
			for(int i=0;i<Hunters.Length;i++)
			{
				if(AllPlayers[x].GetComponent<NetworkIdentity>().netId.ToString()==Hunters[i])
				{
					AllPlayers[x].GetComponent<PlayerSetup>().isHider=false;
					AllPlayers[x].GetComponent<PlayerSetup>().ChangeClassSpectator();
					c++;
					break;
				}
			}
			if(c>0)
				continue;
			for(int i=0;i<Hiders.Length;i++)
			{
				if(AllPlayers[x].GetComponent<NetworkIdentity>().netId.ToString()==Hiders[i])
				{
					AllPlayers[x].GetComponent<PlayerSetup>().isHider=true;
					AllPlayers[x].GetComponent<PlayerSetup>().ChangeClassSpectator();
					break;
				}
			}
		}
	}

	[ClientRpc]
	void RpcSpectator(NetworkIdentity networkIdentity)
	{
		GetComponent<PlayerSetup>().ChangeClassSpectatorAllClients(networkIdentity);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpectateMove : MonoBehaviour {
	
	public float walkSpeed = 0.1f;
	public float runSpeed = 0.3f;
	public float CurrentSpeed=0.0f;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if(PauseMenu.IsOn)
			return;

		if(Input.GetKey(KeyCode.W))//Walk
		{
			transform.Translate(transform.forward * walkSpeed, Space.World);
			CurrentSpeed=walkSpeed;
		}

		if(Input.GetKey(KeyCode.LeftShift)&&Input.GetKey(KeyCode.W))//Run
		{
			transform.Translate(transform.forward * runSpeed, Space.World);
			CurrentSpeed=runSpeed;
		}
	
		if(Input.GetKey(KeyCode.D))//Right
		{
			transform.Translate(transform.right * CurrentSpeed, Space.World);			
		}
		if(Input.GetKey(KeyCode.A))//Left
		{
			transform.Translate(-transform.right * CurrentSpeed, Space.World);
		}
		if(Input.GetKey(KeyCode.S))//Walk BackWard
		{
			transform.Translate(-transform.forward * CurrentSpeed, Space.World);				
		}
		
		if(Input.GetKey(KeyCode.Space))
		{	
			transform.Translate(transform.up * CurrentSpeed, Space.World);		
		}
		if(Input.GetKey(KeyCode.LeftControl))
		{	
			transform.Translate(-transform.up * CurrentSpeed, Space.World);		
		}
	}
}

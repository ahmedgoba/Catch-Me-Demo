﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLock : MonoBehaviour {
	[SerializeField] GameObject ClassChangeDialog;
	// Use this for initialization
	void Update () {
		if(PauseMenu.IsOn||ClassChangeDialog.activeSelf)
		Cursor.visible=true;
		else
		Cursor.visible=false;
		
		//Cursor.lockState=CursorLockMode.Locked;
	}
	void OnDestroy()
	{
		Cursor.visible=true;
	}
}

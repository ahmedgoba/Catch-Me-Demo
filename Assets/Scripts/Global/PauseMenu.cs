﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
public class PauseMenu : MonoBehaviour {

	public static bool IsOn;
	private NetworkManager networkManager;
	public GameObject DialogBox,ChangeClass,OptionsMenu;	

	void Start ()
	{
		networkManager = NetworkManager.singleton;
	}
	public void LeaveRoomRequest()
	{
		DialogBox.SetActive(true);
		gameObject.SetActive(false);
	}
	public void LeaveRoom ()
	{
		MatchInfo matchInfo = networkManager.matchInfo;
		networkManager.matchMaker.DropConnection(matchInfo.networkId, matchInfo.nodeId, 0, networkManager.OnDropConnection);
		networkManager.StopHost();
    }
	public void CancelLeaving()
	{
		DialogBox.SetActive(false);
		gameObject.SetActive(true);
	}
	public void ChangeClassCanvas()
	{
		ChangeClass.SetActive(true);
		gameObject.SetActive(false);
	}
	public void ToggleOptionsWindow()
	{
		OptionsMenu.SetActive(true);
		gameObject.SetActive(false);
	}
}

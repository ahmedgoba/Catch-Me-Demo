﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour {

	[SerializeField] GameObject pauseMenuCanvas,DialogeBox,ChangeClass,Crosshair,OptionsMenu;
	[SerializeField] Text StartTime;
	[SerializeField] GameControl GameControl;
	[SerializeField] Animator animator;
	// Use this for initialization
	void Start () {
		PauseMenu.IsOn = true;
		animator.enabled=false;
		Crosshair.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			if(DialogeBox.activeSelf)
				DialogeBox.SetActive(false);
			else if(ChangeClass.activeSelf)
				ChangeClass.SetActive(false);
			else if(OptionsMenu.activeSelf)
				OptionsMenu.SetActive(false);

			TogglePauseMenu();
		}
	}
	public void TogglePauseMenu ()
	{
		if(pauseMenuCanvas.activeSelf)// close all player UI
		{
			pauseMenuCanvas.SetActive(false);
			PauseMenu.IsOn=false;
			animator.enabled=true;  	// Resume Animations
			Cursor.visible=false; 		// Hide Cursor
			Crosshair.SetActive(true);  // Show Crosshair
		}
		else
		{
			pauseMenuCanvas.SetActive(true);
			PauseMenu.IsOn = true;
			animator.enabled=false;
			Cursor.visible=true;
			Crosshair.SetActive(false);
		}			
    }

	
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpectateLook : MonoBehaviour {

	public float mouseSensitivity = 3.0f;
	float mouseX,mouseY;
	private float rotY = 0.0f; // rotation around y axis
	private float rotX = 0.0f; // rotation around x axis

	void Start ()
	{
		Vector3 rot = transform.localRotation.eulerAngles;
		
		rotY = rot.y;
		rotX = rot.x;
	}

	void Update ()
	{
		if(PauseMenu.IsOn)
			return;

		mouseX = Input.GetAxis("Mouse X");
        mouseY = -Input.GetAxis("Mouse Y");
 
        rotY += mouseX * mouseSensitivity;
        rotX += mouseY * mouseSensitivity;
		
        Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
        transform.rotation = localRotation;
	}
}

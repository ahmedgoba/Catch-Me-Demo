﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerSetup : NetworkBehaviour {
	[SerializeField] Camera Cam;
	public bool isHider=false;
	public bool inSpectate=false;
	[SerializeField] GameObject[] ComponentsToDisable;
	[SerializeField] GameObject PlayerUi;
	[SerializeField] GameObject HunterPrefab=null,HunterCam=null;
	public GameObject HiderPrefab=null,HiderCam=null;
	[SerializeField] GameObject CagePref,SpectatePosition;
	[SerializeField] GameObject ClassChangeDialog;
	GameObject MapObjects;
	public Text TestText;
	[SerializeField] Animator animator;
	GameObject HiderPositions;
	GameObject HunterPositions;
	[SerializeField] Avatar HiderAvatar=null,HunterAvatar=null;

	int HiderHealth=2;
	//bool isCatched=false;
	// Use this for initialization
	void Start () {
		HiderPositions=GameObject.FindGameObjectWithTag("HiderPositions");
		HunterPositions=GameObject.FindGameObjectWithTag("HunterPositions");
		if(!isLocalPlayer)
		{
			for(int x=0;x<ComponentsToDisable.Length;x++)
			{
				ComponentsToDisable[x].gameObject.SetActive(false);
			}
			GetComponent<Move>().enabled=false;
			GetComponent<Shot>().enabled=false;
			GetComponent<GameControl>().enabled=false;
			Cam.GetComponent<look>().enabled=false;
		}
		else
		{
			
			// animator.enabled=false;
			// gameObject.GetComponent<Move>().enabled=false;
			// gameObject.GetComponent<Shot>().enabled=false;
			// Cam.GetComponent<look>().enabled=false;
			
			if(!isServer)
			{
				CmdGetStartingTime();
			}
			else if (isServer&&isLocalPlayer)
			{
				ClassChangeDialog.SetActive(true);
				gameObject.name="Server";
			}
			MapObjects=GameObject.FindGameObjectWithTag("Map");
			//TestText.text=GetComponent<NetworkIdentity>().netId.ToString();
			//StartCoroutine(ChangeClass());	
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(!isLocalPlayer)
		{
			for(int x=0;x<ComponentsToDisable.Length;x++)
			{
				ComponentsToDisable[x].gameObject.SetActive(false);
			}

			GetComponent<Move>().enabled=false;
			GetComponent<Shot>().enabled=false;
			GetComponent<GameControl>().enabled=false;
			Cam.GetComponent<look>().enabled=false;
		}
	}

	void OnDisable(){
		//Destroy(PlayerUIInst);
		//DestroyImmediate(CrossHair,true);
	}
	[Command]
	void CmdGetStartingTime()
	{
		GameObject Server= GameObject.Find("Server");
		float time=Server.GetComponent<GameControl>().StartTimeVar;
		TargetGetStartingTime(connectionToClient,time);
	}
	[TargetRpc]
	void TargetGetStartingTime(NetworkConnection target,float time)
	{
		if(time<=0)
		{
			ActivateSpectateMode();
			GetComponent<GameControl>().isGameStarted=true;
		}
		else
		{
			ClassChangeDialog.SetActive(true);
			GetComponent<GameControl>().StartTimeVar=time;
		}
	}
	void ActivateSpectateMode()
	{
		inSpectate=true;
		SpectatePosition.SetActive(true);

		GetComponent<Rigidbody>().useGravity=false;

		Cam.transform.parent=SpectatePosition.transform;
		Cam.transform.GetChild(0).gameObject.SetActive(false);
		Cam.transform.localRotation=Quaternion.identity;
		Cam.transform.localPosition=Vector3.zero;

		// animator.enabled=false;
		// HunterPrefab.SetActive(false);
		// HiderPrefab.SetActive(false);

		GetComponent<Move>().enabled=false;
		Cam.GetComponent<look>().enabled=false;
		GetComponent<GameControl>().CmdSpectator(GetComponent<NetworkIdentity>());
		GetComponent<GameControl>().StartTimeCanvas.enabled=false;

		PlayerUi.GetComponent<PlayerUI>().TogglePauseMenu();
		PlayerUi.GetComponent<PlayerUI>().TogglePauseMenu();

	}
	public void DeactivateSpectateMode()
	{
		GetComponent<Rigidbody>().useGravity=true;

		Cam.transform.parent=HunterCam.transform;
		Cam.transform.GetChild(0).gameObject.SetActive(false);
		Cam.transform.localRotation=Quaternion.identity;
		Cam.transform.localPosition=Vector3.zero;

		animator.enabled=true;
		HunterPrefab.SetActive(true);
		HiderPrefab.SetActive(false);
		
		animator.avatar=HunterAvatar;
		animator.SetLayerWeight(1,1.0f);
		animator.SetLayerWeight(2,0.0f);

		GetComponent<Move>().enabled=true;
		Cam.GetComponent<look>().enabled=true;
		inSpectate=false;
		SpectatePosition.SetActive(false);
	}
	public void ChangeClass()
	{
		Cam.GetComponent<look>().enabled=true;
		GetComponent<Move>().enabled=true;
		inSpectate=false;
		if(isHider)
		{
			animator.avatar=HiderAvatar;
			animator.SetLayerWeight(1,0.0f);
			animator.SetLayerWeight(2,1.0f);
			Cam.transform.parent=HiderCam.transform;
			Cam.transform.GetChild(0).gameObject.SetActive(false);
			Cam.transform.localRotation=Quaternion.identity;
			Cam.transform.localPosition=Vector3.zero;
			HunterPrefab.SetActive(false);
			HiderPrefab.SetActive(true);
			
			
			if(isLocalPlayer)
			{
				gameObject.layer=LayerMask.NameToLayer("LocalPlayer");
				foreach(Transform GO in GetComponentsInChildren<Transform>())
				{
					GO.gameObject.layer=LayerMask.NameToLayer("LocalPlayer");
				}

				MapObjects.layer=LayerMask.NameToLayer("Light");
				foreach(Transform GO in MapObjects.GetComponentsInChildren<Transform>())
				{
					GO.gameObject.layer=LayerMask.NameToLayer("Light");
				}
			}
			else
			{
				gameObject.layer=LayerMask.NameToLayer("Hider");
				foreach(Transform GO in GetComponentsInChildren<Transform>())
				{
					GO.gameObject.layer=LayerMask.NameToLayer("Hider");
				}
			}
		}
		else
		{
			animator.avatar=HunterAvatar;
			animator.SetLayerWeight(1,1.0f);
			animator.SetLayerWeight(2,0.0f);
			Cam.transform.parent=HunterCam.transform;
			Cam.transform.GetChild(0).gameObject.SetActive(true);
			Cam.transform.localRotation=Quaternion.identity;
			Cam.transform.localPosition=Vector3.zero;
			HunterPrefab.SetActive(true);
			HiderPrefab.SetActive(false);
			

			if(isLocalPlayer)
			{
				gameObject.layer=LayerMask.NameToLayer("LocalPlayer");
				foreach(Transform GO in GetComponentsInChildren<Transform>())
				{
					GO.gameObject.layer=LayerMask.NameToLayer("LocalPlayer");
				}

				MapObjects.layer=LayerMask.NameToLayer("Default");
				foreach(Transform GO in MapObjects.GetComponentsInChildren<Transform>())
				{
					GO.gameObject.layer=LayerMask.NameToLayer("Default");
				}
			}
			else
			{
				gameObject.layer=LayerMask.NameToLayer("RemotePlayer");
				foreach(Transform GO in GetComponentsInChildren<Transform>())
				{
					GO.gameObject.layer=LayerMask.NameToLayer("RemotePlayer");
				}
			}
		}
	}
	public void ChangeClassSpectator()
	{	
		if(isHider)
		{
			animator.avatar=HiderAvatar;
			animator.SetLayerWeight(1,0.0f);
			animator.SetLayerWeight(2,1.0f);
			HunterPrefab.SetActive(false);
			HiderPrefab.SetActive(true);
			Cam.transform.parent=HiderCam.transform;
			Cam.transform.GetChild(0).gameObject.SetActive(false);
			
			gameObject.layer=LayerMask.NameToLayer("RemotePlayer");
			foreach(Transform GO in GetComponentsInChildren<Transform>())
			{
				GO.gameObject.layer=LayerMask.NameToLayer("RemotePlayer");
			}
		}
		else if (inSpectate)
		{
			HunterPrefab.SetActive(false);
			HiderPrefab.SetActive(false);
		}
		else		
		{
			animator.avatar=HunterAvatar;
			animator.SetLayerWeight(1,1.0f);
			animator.SetLayerWeight(2,0.0f);
			HunterPrefab.SetActive(true);
			HiderPrefab.SetActive(false);
			Cam.transform.parent=HunterCam.transform;
			Cam.transform.GetChild(0).gameObject.SetActive(true);

			gameObject.layer=LayerMask.NameToLayer("RemotePlayer");
			foreach(Transform GO in GetComponentsInChildren<Transform>())
			{
				GO.gameObject.layer=LayerMask.NameToLayer("RemotePlayer");
			}
		}
		
	}

	public void ChangeClassSpectatorAllClients(NetworkIdentity networkIdentity)
	{
		GameObject Player=(ClientScene.FindLocalObject(networkIdentity.netId));
		Player.GetComponent<PlayerSetup>().inSpectate=true;
		Player.GetComponent<PlayerSetup>().HunterPrefab.SetActive(false);
		Player.GetComponent<PlayerSetup>().HiderPrefab.SetActive(false);
	}
	void SetPositions(int positionIndex)
	{
		Cam.transform.localRotation=Quaternion.identity;
		Cam.transform.localPosition=Vector3.zero;
		if(isHider)
		{
			transform.position=HiderPositions.transform.GetChild(positionIndex).position;
		}
		else
		{
			transform.position=HunterPositions.transform.GetChild(positionIndex).position;
		}
	}
	public IEnumerator StartGame(int positionIndex)
	{
		HiderHealth=2;
		SetPositions(positionIndex);
		GetComponent<Move>().enabled=false;
		yield return new WaitForSeconds(5.0f);
		GetComponent<Move>().enabled=true;
		GetComponent<Shot>().enabled=true;
		//run start game alarm sound
		GetComponent<GameControl>().isGameStarted=true;		
	}
	public IEnumerator Catch(LineRenderer LR,GameObject Weapon)
	{
		gameObject.GetComponent<Move>().enabled=false;
		if(LR.enabled)
			Cam.GetComponent<look>().enabled=false;
		if(GetComponent<PlayerSetup>().isHider&&GetComponent<GameControl>().isGameStarted)
		{
			HiderHealth--;
			
			if(HiderHealth<=0)
			{
				if(isLocalPlayer)
				{
					//Cam.GetComponent<look>().enabled=false;
					GetComponent<Shot>().enabled=false;
					GetComponent<GameControl>().CmdHiderDead(GetComponent<NetworkIdentity>());
					CmdInCage(GetComponent<NetworkIdentity>());
					yield break;
				}
			}
			
		}
		yield return new WaitForSeconds(5f);
		if(HiderHealth<=0)
		{
			if(isLocalPlayer)
			{
				GetComponent<Shot>().enabled=false;
				//Cam.GetComponent<look>().enabled=false;
				yield break;
			}
		}
			

		// Hider Escaped
		gameObject.GetComponent<Move>().enabled=true;
		Cam.GetComponent<look>().enabled=true;
		HiderHealth=2;

		if(LR.enabled)
		{
			LR.SetPosition(1,Weapon.transform.position);
			LR.SetPosition(0,Weapon.transform.position);
			LR.enabled=false;
		}
		else
			GetComponent<Shot>().Rope.SetActive(false);
	}

	public void Stun(NetworkInstanceId id)
	{
		if(!isLocalPlayer)
			return;
		Cam.GetComponent<StunEffect>().enabled=false;

		Cam.GetComponent<StunEffect>().enabled=true;
	}

	[Command]
	public void CmdInCage(NetworkIdentity networkIdentity)
	{
		GameObject Cage= Instantiate(CagePref,transform.position,Quaternion.identity);
		NetworkServer.Spawn(Cage);		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class StunEffect : MonoBehaviour {
	[SerializeField] RawImage ScreenShotImage;
	[SerializeField] RawImage WhiteImage;
	[SerializeField] Behaviour moveScript;
	[SerializeField] AudioSource StunSound;
	public bool grab,stunned;
	public Color color;

	float BlindTime;
	float StunTime;

	void OnEnable()
    {
        ScreenShotImage.texture=null;

		color=ScreenShotImage.color;
		color.a=0.0f;
		ScreenShotImage.color=color;
	
		color=WhiteImage.color;
		color.a=0.0f;
		WhiteImage.color=color;

		BlindTime=1.0f;
		StunTime=4.0f;

		stunned=true;
		grab=true;
    }
	// Use this for initialization
	void Start () {		
	}
	
	// Update is called once per frame
	void Update () {
		if(!stunned)
		{
			StopStun();
		}
		else if(stunned&&!grab)
		{
			StunCoolDown();
		}
		
	}
	private void OnPostRender()
    {
        if (grab)
        {
            //Create a new texture with the width and height of the screen
            Texture2D texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
            //Read the pixels in the Rect starting at 0,0 and ending at the screen's width and height
            texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, false);
            texture.Apply();
            //Check that the display field has been assigned in the Inspector
            if (ScreenShotImage != null)
                //Give your GameObject with the renderer this texture
                ScreenShotImage.texture=texture;
            //Reset the grab state
            grab = false;
			//Stun Effect
			StartStun();
        }
    }
	void StartStun()
	{
		StunSound.Play();
		color=ScreenShotImage.color;
		color.a=1f;
		ScreenShotImage.color=color;

		color=WhiteImage.color;
		color.a=0.9f;
		WhiteImage.color=color;

		moveScript.enabled=false;
	}
	public void StunCoolDown()
	{
		
		StunTime-=Time.deltaTime;
		if(StunTime<=0.0f)
		{
			StunTime=4.0f;
			stunned=false;
		}

	}
	public void StopStun()
	{
		StunSound.Stop();
		color=ScreenShotImage.color;
		color.a-=Time.deltaTime;
		ScreenShotImage.color=color;

		color=WhiteImage.color;
		color.a-=Time.deltaTime;
		WhiteImage.color=color;

		BlindTime-=Time.deltaTime;
		if(BlindTime<=0.0f)
		{
			BlindTime=1.0f;
			moveScript.enabled=true;
			enabled=false;
		}
	}
}

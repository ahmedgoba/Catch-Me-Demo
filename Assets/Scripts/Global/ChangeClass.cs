﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ChangeClass : NetworkBehaviour {
	[SerializeField] GameObject CurrentPlayer;
	[SerializeField] GameObject PlayerUI;
	[SerializeField] Camera Cam;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void ChooseHunter()
	{
		CurrentPlayer.GetComponent<PlayerSetup>().isHider=false;

		// CurrentPlayer.GetComponent<Animator>().enabled=true;
		// CurrentPlayer.GetComponent<Move>().enabled=true;
		//CurrentPlayer.GetComponent<Shot>().enabled=true;
		// Cam.GetComponent<look>().enabled=true;
		
		CurrentPlayer.GetComponent<GameControl>().CmdSendChosenClass(CurrentPlayer.GetComponent<NetworkIdentity>(),false);

		gameObject.SetActive(false);
		PlayerUI.GetComponent<PlayerUI>().TogglePauseMenu();
		PlayerUI.GetComponent<PlayerUI>().TogglePauseMenu();
		// PauseMenu.IsOn=false;
		// Cursor.visible=false;
		// CurrentPlayer.GetComponent<Animator>().enabled=true;
	}

	public void ChooseHider()
	{
		CurrentPlayer.GetComponent<PlayerSetup>().isHider=true;

		// CurrentPlayer.GetComponent<Animator>().enabled=true;
		// CurrentPlayer.GetComponent<Move>().enabled=true;
		//CurrentPlayer.GetComponent<Shot>().enabled=true;
		// Cam.GetComponent<look>().enabled=true;

		CurrentPlayer.GetComponent<GameControl>().CmdSendChosenClass(CurrentPlayer.GetComponent<NetworkIdentity>(),true);

		gameObject.SetActive(false);
		PlayerUI.GetComponent<PlayerUI>().TogglePauseMenu();
		PlayerUI.GetComponent<PlayerUI>().TogglePauseMenu();
		// PauseMenu.IsOn=false;
		// Cursor.visible=false;
		// CurrentPlayer.GetComponent<Animator>().enabled=true;		
	}
}
